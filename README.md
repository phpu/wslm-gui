WSL管理工具
===============

[![WSL管理工具](https://img.shields.io/badge/stable-v4.0.1-blue.svg)](https://gitee.com/itxq/wslm-gui.git)
[![Python Version](https://img.shields.io/badge/Python-3.7+-brightgreen.svg)](https://www.python.org/)
[![wslm Version](https://img.shields.io/badge/wslm-v1.0.1+-important.svg)](https://gitee.com/itxq/wslm.git)


### 打包

```
pyinstaller WSL管理工具.py -F -w --hidden-import PySide2.QtXml --hidden-import wslm_gui --icon="resource/icon/logo.ico" --add-data="resource\wslm.ui;resource"
```

### 启动脚本示例

```
@echo off
start /B x410.exe /wm
wsl.exe "/usr/bin/wsl-start" -u root
```

### 部分界面预览

- 功能展示

![界面预览](./images/界面预览.gif)

- 配置管理

![配置管理](./images/配置管理.gif)

- 添加端口

![添加端口](./images/添加端口.gif)

- 删除端口
  
![删除端口](./images/删除端口.gif)

- 清除端口

![清除端口](./images/清除端口.gif)

- 终止系统

![终止系统](./images/终止系统.gif)