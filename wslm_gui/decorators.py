"""
decorators.py
By IT小强xqitw.cn <mail@xqitw.cn>
At 6/19/21 8:02 AM
"""
from functools import wraps
from .utils import signal_emit_text


def signal_emit_text_ui(signal, fb, text, is_end=False):
    """
    推送字符串数据
    :param signal:
    :param fb:
    :param text:
    :param is_end:
    :return:
    """

    def _wraps(func):
        @wraps(func)
        def __wraps(self, *args, **kwargs):
            signal_fb = getattr(getattr(self, 'ui'), fb)
            if not is_end:
                signal_emit_text(signal, signal_fb, text)
            _result = func(self, *args, **kwargs)
            if is_end:
                signal_emit_text(signal, signal_fb, text)
            return _result

        return __wraps

    return _wraps
