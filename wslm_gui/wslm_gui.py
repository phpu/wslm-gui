"""
wslm_gui.py 启动入口
By IT小强xqitw.cn <mail@xqitw.cn>
At 6/16/21 6:17 PM
"""

from PySide2.QtWidgets import QApplication

from .qt_application import QtApplication


def main():
    app = QApplication([])
    qt_application = QtApplication(app)
    qt_application.auto_show()
    app.exec_()


if __name__ == "__main__":
    main()
